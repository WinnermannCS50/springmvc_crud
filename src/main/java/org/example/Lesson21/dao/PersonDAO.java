package org.example.Lesson21.dao;

import org.example.Lesson21.models.Person;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * @Component - создает Бин класса
 */
@Component
public class PersonDAO {
    /**
     * PEOPLE_COUNT - переменная для id, изначально равна нулю
     * ++PEOPLE_COUNT - инкрементируется каждый раз на 1
     */
    private static int PEOPLE_COUNT;
    private List<Person> people;
    {
        people = new ArrayList<>();
        people.add(new Person(++PEOPLE_COUNT,"Tom"));
        people.add(new Person(++PEOPLE_COUNT,"Bob"));
        people.add(new Person(++PEOPLE_COUNT,"Mike"));
        people.add(new Person(++PEOPLE_COUNT,"Katy"));
    }

    /**
     * метод List<Person> index() - возвращает список из людей
     * @return
     */
    public List<Person> index(){
        return people;
    }

    public Person show(int id){
        /**
         * метод Person show(int id) - возвращает одного человека, принимает на вход id-этого человека.
         * Находит по id человека из БД(private List<Person> people) и возвращает этого человека
         *
         * filter(person -> person.getId()==id) - отфильтровывает человека по его id (что бы id равнялся тому id, кот. пришел в качестве аргумента )
         * findAny() - находит человека, если он есть
         * orElse(null) - возвращает null, если человека с таким id нет
         */
        return people.stream().filter(person -> person.getId()==id).findAny().orElse(null);
    }
}
